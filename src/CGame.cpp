#include "CGame.h"
#include "CPlayer.h"
#include "CHuman.h"
#include "CAI.h"
#include "CCard.h"
#include "CCardOffensive.h"
#include "CCardDefensive.h"
#include "CCardSupport.h"
#include <vector>
#include <fstream>
#include <sstream>
#include <ncurses.h>

CGame::CGame(std::string nameOne, std::vector<CCard *> deckOne, std::string nameTwo, std::vector<CCard *> deckTwo,
             bool multiplayer, bool cheats) {
    CPlayer *playerOne;
    CPlayer *playerTwo;
    if (multiplayer) {
        playerOne = new CHuman(nameOne, deckOne, 300, 200, 1, 0, this);
        playerTwo = new CHuman(nameTwo, deckTwo, 300, 200, 1, 0, this);
    } else if (cheats) {
        playerOne = new CHuman(nameOne, deckOne, 300, 200, 1, 0, this);
        playerTwo = new CAI(nameTwo, deckTwo, 600, 400, 1, 0, this);
    } else {
        playerOne = new CHuman(nameOne, deckOne, 300, 200, 1, 0, this);
        playerTwo = new CAI(nameTwo, deckTwo, 300, 200, 1, 0, this);
    }
    m_players.push_back(playerOne);
    m_players.push_back(playerTwo);
    for (auto player:m_players) {
        for (auto card:player->GetDeck()) {
            card->SetOwner(player);
        }
    }
    m_turn = 0;
}

CPlayer *CGame::GetEnemyPlayer(CPlayer *me) {
    for (auto &item:m_players) {
        if (item != me) {
            return item;
        }
    }
    return nullptr;
}

std::vector<CCard *> CGame::GetEnemyCards(CPlayer *me) {
    for (auto &item:m_players) {
        if (item != me) {
            return item->GetDeck();
        }
    }
    std::vector<CCard *> empty;
    return empty;
}

CGame::~CGame() {
    for (auto &item:m_players) {
        delete item;
    }
}

bool CGame::GameOver() {
    for (auto &item:m_players) {
        if (item->GetHealth() <= 0 || item->GetDeck().empty()) {
            return true;
        }
    }
    return false;
}

void CGame::Play() {
    while (!GameOver()) {
        for (auto &item:m_players) {
            InsertToLog(item->GetName() + " is now playing");
            item->Play();
            item->SetEnergy(item->GetEnergy() + 1);
            m_last++;
        }
        InsertToLog("Turn " + std::to_string(m_turn) + " is now over");
        m_turn++;
    }
    int width;
    int height;
    getmaxyx(stdscr, width, height);
    WINDOW *gameOverWin = newwin(width, height, 0, 0);
    box(gameOverWin, 0, 0);
    refresh();
    wrefresh(gameOverWin);
    for (const auto &player:m_players) {
        if (player->GetHealth() > 0 && !player->GetDeck().empty()) {
            mvwprintw(gameOverWin, width / 2, height / 2, "Player %s won", player->GetName().c_str());
            break;
        }
    }
    wgetch(gameOverWin);
    wclear(gameOverWin);
    wrefresh(gameOverWin);
    delwin(gameOverWin);
}

void CGame::ClearCards() {
    for (auto &item:m_players) {
        std::vector<CCard *> toRemove;
        for (auto &card:item->GetDeck()) {
            if (card->GetHealth() <= 0) {
                toRemove.push_back(card);
            }
        }
        for (auto &card:toRemove) {
            item->RemoveFromDeck(card);
        }
    }
}

void CGame::InsertToLog(std::string message) {
    m_logMessages.push_back(message);
    WINDOW *logWin = newwin(8, 83, 16, 26);
    box(logWin, 0, 0);
    refresh();
    wrefresh(logWin);
    auto it = m_logMessages.rbegin();
    for (int i = 1; i < 7; ++i) {
        if (it != m_logMessages.rend()) {
            mvwprintw(logWin, i, 2, it->c_str());
            it++;
        }
    }
    wrefresh(logWin);
    delwin(logWin);
}

void CGame::SaveGame() {
    int aX = 1;
    int aY = 16;
    echo();
    WINDOW *saveWin = newwin(7, 25, aY, aX);
    box(saveWin, 0, 0);
    refresh();
    mvwprintw(saveWin, 1, 2, "Save name:");
    char gameName[25];
    mvwgetnstr(saveWin, 2, 2, gameName, 24);
    wclear(saveWin);
    wrefresh(saveWin);
    delwin(saveWin);
    noecho();
    std::ofstream saves("examples/saves.csv", std::ios::app);
    if (saves.is_open()) {
        saves << gameName << "\n";
    }
    saves.close();
    std::ofstream saveGame;
    std::string name = gameName;
    name = "examples/" + name + ".crpg";
    saveGame.open(name);
    if (saveGame.good()) {
        saveGame << "#" << m_turn << "\n";
        saveGame << "$" << m_last << "\n";
        for (const auto &player:m_players) {
            if (dynamic_cast<CHuman *>(player)) {
                saveGame << "+";
            } else {
                saveGame << "!";
            }
            saveGame << player->GetName()
                     << "," << player->GetHealth()
                     << "," << player->GetArmor()
                     << "," << player->GetEnergy()
                     << "," << player->GetUsedEnergy()
                     << "\n";
            for (const auto &card:player->GetDeck()) {
                if (dynamic_cast<CCardOffensive *>(card)) {
                    saveGame << "*";
                } else if (dynamic_cast<CCardDefensive *>(card)) {
                    saveGame << "~";
                } else {
                    saveGame << "&";
                }
                saveGame << card->GetId() << ","
                         << card->GetName() << ","
                         << card->GetHealth() << ","
                         << card->GetArmor() << ","
                         << card->GetStrength()
                         << "\n";
            }
            saveGame << "@" << "\n";
        }
        saveGame.close();
    }
}

CGame::CGame(std::string save) {
    bool failed = false;
    save = "examples/" + save + ".crpg";
    std::ifstream stream(save);
    if (stream.is_open()) {
        std::string line;
        CPlayer *player = nullptr;
        std::vector<CCard *> cards;
        while (getline(stream, line)) {
            if (line[0] == '#') {
                line = line.substr(1, line.size());
                m_turn = std::stoi(line);
                continue;
            }
            if (line[0] == '$') {
                line = line.substr(1, line.size());
                m_last = std::stoi(line);
                continue;
            }
            if (line[0] == '+' || line[0] == '!') {
                bool human = false;
                if (line[0] == '+') {
                    human = true;
                }
                line = line.substr(1, line.size());
                std::vector<std::string> values;
                std::stringstream ss(line);
                while (ss.good()) {
                    std::string sub;
                    getline(ss, sub, ',');
                    values.push_back(sub);
                }
                if (values.size() != 5) {
                    failed = true;
                    break;
                }
                std::string name;
                int hp;
                int armor;
                int energy;
                int usedEnergy;
                name = values[0];
                hp = std::stoi(values[1]);
                armor = std::stoi(values[2]);
                energy = std::stoi(values[3]);
                usedEnergy = std::stoi(values[4]);
                if (human) {
                    player = new CHuman(name, hp, armor, energy, usedEnergy, this);
                } else {
                    player = new CAI(name, hp, armor, energy, usedEnergy, this);
                }
                continue;
            }
            if (line[0] == '*' || line[0] == '~' || line[0] == '&') {
                int type = 0;
                if (line[0] == '~') {
                    type = 1;
                } else if (line[0] == '&') {
                    type = 2;
                }

                line = line.substr(1, line.size());
                std::vector<std::string> values;
                std::stringstream ss(line);
                while (ss.good()) {
                    std::string sub;
                    getline(ss, sub, ',');
                    values.push_back(sub);
                }
                if (values.size() != 5) {
                    failed = true;
                    break;
                }
                std::string name;
                int id;
                int hp;
                int armor;
                int strength;
                id = std::stoi(values[0]);
                name = values[1];
                hp = std::stoi(values[2]);
                armor = std::stoi(values[3]);
                strength = std::stoi(values[4]);
                CCard *card = nullptr;
                switch (type) {
                    case 0:
                        card = new CCardOffensive(id, name, hp, armor, strength);
                        break;
                    case 1:
                        card = new CCardDefensive(id, name, hp, armor, strength);
                        break;
                    case 2:
                        card = new CCardSupport(id, name, hp, armor, strength);
                        break;
                    default:
                        break;
                }
                cards.push_back(card);
                continue;
            }
            if (line[0] == '@') {
                if (player != nullptr) {
                    player->SetDeck(cards);
                } else {
                    failed = true;
                    break;
                }
                m_players.push_back(player);
                cards.clear();
                continue;
            }
        }
    }
    if (failed) {
        mvwprintw(stdscr, 0, 0, "failed to load %s press any key to continue", save.c_str());
        wgetch(stdscr);
    }
}


