#include "CAI.h"
#include <algorithm>
#include "CCardSupport.h"
#include "CCardDefensive.h"
#include <random>
#include <thread>

CAI::CAI(std::string name, std::vector<CCard *> cards, int health, int armor, int energy, int usedEnergy, CGame *game) {
    m_name = name;
    m_deck = cards;
    m_health = health;
    m_armor = armor;
    m_energy = energy;
    m_usedEnergy = usedEnergy;
    m_game = game;
}

CAI::CAI(std::string name, int health, int armor, int energy, int usedEnergy, CGame *game) {
    m_name = name;
    m_health = health;
    m_armor = armor;
    m_game = game;
    m_energy = energy;
    m_usedEnergy = usedEnergy;
}


void CAI::Play() {
    m_game->ClearCards();
    if (m_game->GameOver()) {
        return;
    }
    std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(1, 100);
    while (m_usedEnergy < m_energy) {
        if (m_game->GetEnemyPlayer(this)->GetHealth() <= m_health) {
            Attack(distribution(generator) < 90);
        } else if (m_fullHealth / 2 >= m_health) {
            Defend();
        } else {
            if (distribution(generator) < 75) {
                Attack(distribution(generator) < 40);
            } else {
                Defend();
            }
        }
        m_game->ClearCards();
        if (m_game->GameOver()) {
            return;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(800));
        m_usedEnergy++;
    }
    m_usedEnergy = 0;
}

void CAI::Attack(bool player) {
    std::sort(m_deck.begin(), m_deck.end(),
              [](CCard *one, CCard *two) { return one->GetAttackRating() > two->GetAttackRating(); });
    if (player) {
        m_deck.front()->Attack(m_game->GetEnemyPlayer(this));
        return;
    }
    auto enemy_deck = m_game->GetEnemyPlayer(this)->GetDeck();
    std::sort(enemy_deck.begin(), enemy_deck.end(),
              [](CCard *one, CCard *two) { return one->GetAttackRating() > two->GetAttackRating(); });
    m_deck.front()->Attack(enemy_deck.front());
}

void CAI::Defend() {
    //If support or defensive cards are available use them on yourself
    std::sort(m_deck.begin(), m_deck.end(),
              [](CCard *one, CCard *two) { return one->GetSupportRating() > two->GetSupportRating(); });
    for (const auto &item:m_deck) {
        if (dynamic_cast<CCardDefensive *>(item) || dynamic_cast<CCardSupport *>(item)) {
            item->Special(this);
            return;
        }
    }
    //If that fails, protect yourself by trying to eliminate the most dangerous enemy card
    auto enemy_deck = m_game->GetEnemyPlayer(this)->GetDeck();
    std::sort(enemy_deck.begin(), enemy_deck.end(),
              [](CCard *one, CCard *two) { return one->GetAttackRating() > two->GetAttackRating(); });
    m_deck.front()->Attack(enemy_deck.front());
}

CAI::~CAI() {
    for (auto &item:m_deck) {
        delete item;
    }
    m_deck.clear();
}


