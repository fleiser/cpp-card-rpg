#ifndef CCARD_H
#define CCARD_H

#include <string>
#include <iostream>
#include "CPlayer.h"
#include <ncurses.h>

class CPlayer;

/** \brief Class serves as a template for specialized cards */
class CCard {
public:

    /** \brief Destructor */
    virtual ~CCard();

    /** \brief Card attack on a specified card */
    bool Attack(CCard *card);

    /** \brief Card attack on a specified player */
    virtual bool Attack(CPlayer *player) = 0;

    /** \brief Card uses its special ability on a specified card */
    virtual void Special(CCard *card) = 0;

    /** \brief  Card uses its special ability on the player*/
    virtual void Special(CPlayer *player) = 0;

    /** \brief Return cards health */
    int GetHealth();

    /** \brief Return cards armor */
    int GetArmor();

    /** \brief Return cards strength */
    int GetStrength();

    /** \brief Return cards name */
    std::string GetName();

    /** \brief Set cards health */
    bool SetHealth(int health);

    /** \brief Set cards armor */
    void SetArmor(int armor);

    /** \brief Set cards strength */
    void SetStrength(int strength);

    /** \brief  Print the card in a specified window */
    virtual void Print(WINDOW *window, int index) const =0;

    /** \brief Return cards id*/
    int GetId();

    /** \brief Returns a number based on how good of a offensive card this is */
    double GetAttackRating();

    /** \brief Returns a number based on how good of a supportive card this is */
    double GetSupportRating();

    /** \brief Set cards owner */
    void SetOwner(CPlayer *owner);

    /** \brief Destructor */
    CPlayer *GetOwner();

protected:
    int m_id;
    int m_health;
    int m_strength;
    int m_armor;
    std::string m_name;
    CPlayer *m_owner = nullptr;
};


#endif

