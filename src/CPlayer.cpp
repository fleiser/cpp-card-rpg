#include "CPlayer.h"
#include "CCard.h"
#include <string>
#include <algorithm>

CPlayer::~CPlayer() {
    for (auto &item:m_deck) {
        delete item;
    }
    m_deck.clear();
}

int CPlayer::GetHealth() {
    return m_health;
}

int CPlayer::GetArmor() {
    return m_armor;
}

std::vector<CCard *> CPlayer::GetDeck() {
    return m_deck;
}

void CPlayer::SetDeck(std::vector<CCard *> cards) {
    for (auto &item:m_deck) {
        delete item;
    }
    m_deck.clear();
    m_deck = cards;
}

void CPlayer::SetArmor(int armor) {
    if (armor > 0)
        m_armor = armor;
    else
        m_armor = 0;
}

bool CPlayer::SetHealth(int health) {
    if (health > 0) {
        m_health = health;
        return false;
    }
    m_health = 0;
    return true;
}

void CPlayer::SetEnergy(int energy) {
    if (energy > 0)
        m_energy = energy;
    else
        m_energy = 0;
}

int CPlayer::GetEnergy() {
    return m_energy;
}

void CPlayer::RemoveFromDeck(CCard *card) {
    m_deck.erase(std::find(m_deck.begin(), m_deck.end(), card));
}

std::string CPlayer::GetName() {
    return m_name;
}

WINDOW *CPlayer::ShowPlayer(CPlayer *player, int w, int h, int x, int y) {
    WINDOW *playerWindow = newwin(h, w, y, x);
    box(playerWindow, 0, 0);
    mvwprintw(playerWindow, 1, 2, player->m_name.c_str());
    mvwprintw(playerWindow, 2, 2, "Health: %i", player->m_health);
    mvwprintw(playerWindow, 3, 2, "Armor: %i", player->m_armor);
    mvwprintw(playerWindow, 4, 2, "Energy: %i", player->m_energy);
    mvwprintw(playerWindow, 5, 2, "Used: %i", player->m_usedEnergy);
    mvwprintw(playerWindow, 6, 2, "Cards:");
    int index = 7;
    for (const auto &card:player->m_deck) {
        card->Print(playerWindow, index);
        index++;
    }
    refresh();
    wrefresh(playerWindow);
    return playerWindow;
}

CGame *CPlayer::GetGame() {
    return m_game;
}

int CPlayer::GetUsedEnergy() {
    return m_usedEnergy;
}

void CPlayer::SetUsedEnergy(int used) {
    m_usedEnergy = used;
}
