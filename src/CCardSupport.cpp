#include "CCard.h"
#include "CCardSupport.h"

CCardSupport::CCardSupport(int id, std::string name, int health, int armor, int strength) {
    m_id = id;
    m_name = name;
    m_health = health;
    m_armor = armor;
    m_strength = strength;
}

void CCardSupport::Special(CCard *card) {
    card->SetHealth(card->GetHealth() + 5 + (2 * m_health));
    SetHealth(GetHealth() - 2);
    if (m_owner != nullptr) {
        m_owner->GetGame()->InsertToLog(
                m_name + " increased " + card->GetName() + " health by " + std::to_string(5 + (2 * m_health)) +
                " suffering 2 damage");
    }
}

void CCardSupport::Special(CPlayer *player) {
    player->SetHealth(player->GetHealth() + 10 + (2 * m_health));
    SetHealth(GetHealth() - 2);
    if (m_owner != nullptr) {
        m_owner->GetGame()->InsertToLog(
                m_name + " increased " + player->GetName() + " health by " + std::to_string(10 + (2 * m_health)) +
                " suffering 2 damage");
    }
}

bool CCardSupport::Attack(CPlayer *player) {
    int afterArmor = (m_strength / 4) * (m_health / 4) - player->GetArmor();
    if (afterArmor < 0) {
        afterArmor = 0;
    }
    player->SetArmor(player->GetArmor() - (m_strength / 4) * (m_health / 4));
    if (m_owner != nullptr) {
        m_owner->GetGame()->InsertToLog(
                m_name + " attacks " + player->GetName() + " dealing " + std::to_string(afterArmor) + " damage");
    }
    return player->SetHealth(player->GetHealth() - afterArmor);
}

void CCardSupport::Print(WINDOW *window, int index) const {
    mvwprintw(window, index, 3, "-S %s h: %i | a: %i | s: %i ", m_name.c_str(), m_health, m_armor, m_strength);
}


