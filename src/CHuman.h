#ifndef CHUMAN_H
#define CHUMAN_H

#include "CPlayer.h"
#include <string>
#include <ncurses.h>

/** \brief Class representing a human player*/
class CHuman : public CPlayer {
public:
    /** \brief  Play*/
    virtual void Play();

    /** \brief  Destructor*/
    virtual ~CHuman();

    /** \brief  Standard constructor */
    CHuman(std::string name, std::vector<CCard *> cards, int health, int armor, int energy, int usedEnergy,
           CGame *game);

    /** \brief Loading constructor */
    CHuman(std::string name, int health, int armor, int energy, int usedEnergy, CGame *game);

private:
    /** \brief  Player action selection menu */
    void ActionMenu();

    /** \brief  Player card selection menu */
    CCard *SelectCardMenu(CPlayer *player);
};


#endif

