#ifndef CGAME_H
#define CGAME_H

#include "CPlayer.h"
#include "CCard.h"
#include <iostream>
#include <vector>
#include <ncurses.h>

class CCard;

class CPlayer;

/** \brief Class represents a single game, holds players and keeps track of the game state*/
class CGame {
public:
    /** \brief Standard constructor */
    CGame(std::string nameOne, std::vector<CCard *> deckOne, std::string nameTwo, std::vector<CCard *> deckTwo,
          bool multiplayer, bool cheats);

    /** \brief Saved game constructor */
    explicit CGame(std::string save);

    /** \brief Destructor */
    ~CGame();

    /** \brief Return the players opponent */
    CPlayer *GetEnemyPlayer(CPlayer *me);

    /** \brief Return the players opponents cards  */
    std::vector<CCard *> GetEnemyCards(CPlayer *me);

    /** \brief Start the game */
    void Play();

    /** \brief Remove all dead cards */
    void ClearCards();

    /** \brief Check if someone lost */
    bool GameOver();

    /** \brief Insert a message to game log */
    void InsertToLog(std::string message);

    /** \brief Save the game */
    void SaveGame();

private:
    std::vector<CPlayer *> m_players;
    std::vector<std::string> m_logMessages;
    int m_turn = 1;
    int m_last = 1;
};

#endif


