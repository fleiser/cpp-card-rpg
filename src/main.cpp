#include "CMenu.h"
#include "CPlayer.h"
#include "CHuman.h"
#include "CCard.h"
#include "CCardOffensive.h"
#include "CCardDefensive.h"
#include "CCardSupport.h"
#include "CAI.h"
#include "CGame.h"
#include <ncurses.h>

int main() {
    CMenu menu;
    initscr();
    noecho();
    menu.ShowMenu();
    endwin();
    return 0;
}