#ifndef CMENU_H
#define CMENU_H

#include <map>
#include "CCard.h"

/** \brief Class handles storing existing cards and decks. Provides the main menu and takes care of starting new games. */
class CMenu {
public:
    CMenu();

    ~CMenu();

    /** \brief Load all existing cards from a file */
    bool LoadCards();

    /** \brief Load all existing decks from a file */
    bool LoadDecks();

    /** \brief Display saved games menu, loads games. If no saved games exist return to main menu */
    void LoadGames();

    /** \brief Creates a new game*/
    void Play();

    /** \brief Creates a new game against a computer controlled oponnent */
    void PlayVsPc();

    /** \brief Resume a saved game */
    void ResumeGame(std::string game);

    /** \brief Display main menu */
    void ShowMenu();

    /** \brief Deck selection menu */
    void SelectDeck(std::vector<CCard *> *rDeck);

private:
    /** \brief Holds all known cards */
    std::map<int, CCard *> m_cards;
    /** \brief Holds all known decks */
    std::map<std::string, std::vector<CCard *>> m_decks;

    /** \brief Makes sure the terminal has sufficient size to display the game */
    bool ScreenOk();
};

#endif
