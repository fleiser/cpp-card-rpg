#include "CCard.h"

std::string CCard::GetName() {
    return m_name;
}

int CCard::GetStrength() {
    return m_strength;
}

int CCard::GetArmor() {
    return m_armor;
}

int CCard::GetHealth() {
    return m_health;
}

bool CCard::SetHealth(int health) {
    if (health > 0) {
        m_health = health;
        return false;
    }
    m_health = 0;
    return true;
}

void CCard::SetArmor(int armor) {
    if (armor > 0)
        m_armor = armor;
    else
        m_armor = 0;
}

void CCard::SetStrength(int strength) {
    if (strength < 0) {
        strength = 0;
    }
    m_strength = strength;
}

bool CCard::Attack(CCard *card) {
    int afterArmor = m_strength - card->GetArmor();
    if (afterArmor < 0) {
        afterArmor = 0;
    }

    card->SetArmor(card->GetArmor() - m_strength);
    if (m_owner != nullptr) {
        m_owner->GetGame()->InsertToLog(
                m_name + " attacks " + card->GetName() + " dealing " + std::to_string(afterArmor) + " damage");
    }
    return card->SetHealth(card->GetHealth() - afterArmor);
}

double CCard::GetAttackRating() {
    return m_strength + 0.5 * m_health + 0.2 * m_armor;
}

double CCard::GetSupportRating() {
    return m_health + 0.5 * m_strength + 0.2 * m_armor;
}

int CCard::GetId() {
    return m_id;
}

void CCard::SetOwner(CPlayer *owner) {
    m_owner = owner;
}

CPlayer *CCard::GetOwner() {
    return m_owner;
}

CCard::~CCard() {

}

