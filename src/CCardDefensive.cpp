#include "CCard.h"
#include "CCardDefensive.h"

CCardDefensive::CCardDefensive(int id, std::string name, int health, int armor, int strength) {
    m_id = id;
    m_name = name;
    m_health = health;
    m_armor = armor;
    m_strength = strength;
}

void CCardDefensive::Special(CCard *card) {
    card->SetArmor(card->GetArmor() + m_strength + (2 * m_health));
    SetHealth(GetHealth() - 2);
    if (m_owner != nullptr) {
        m_owner->GetGame()->InsertToLog(
                m_name + " increased " + card->GetName() + " armor by " + std::to_string(m_strength + (2 * m_health)) +
                " suffering 2 damage");
    }
}

void CCardDefensive::Special(CPlayer *player) {
    player->SetArmor(player->GetArmor() + m_strength + (2 * m_health));
    SetHealth(GetHealth() - 2);
    if (m_owner != nullptr) {
        m_owner->GetGame()->InsertToLog(m_name + " increased " + player->GetName() + " armor by " +
                                        std::to_string(m_strength + (2 * m_health)) + " suffering 2 damage");
    }
}

bool CCardDefensive::Attack(CPlayer *player) {
    int afterArmor = (m_strength / 2) * (m_health / 4) - player->GetArmor();
    if (afterArmor < 0) {
        afterArmor = 0;
    }
    player->SetArmor(player->GetArmor() - (m_strength / 2) * (m_health / 4));
    if (m_owner != nullptr) {
        m_owner->GetGame()->InsertToLog(
                m_name + " attacks " + player->GetName() + " dealing " + std::to_string(afterArmor) + " damage");
    }
    return player->SetHealth(player->GetHealth() - afterArmor);
}

void CCardDefensive::Print(WINDOW *window, int index) const {
    mvwprintw(window, index, 3, "-D %s h: %i | a: %i | s: %i ", m_name.c_str(), m_health, m_armor, m_strength);
}


