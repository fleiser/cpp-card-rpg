#ifndef CCARDSUPPORT_H
#define CCARDSUPPORT_H

#include "CCard.h"
#include <string>
#include "CPlayer.h"
#include <ncurses.h>

/** \brief Class represents cards with supportive qualities*/
class CCardSupport : public CCard {
public:
    /** \brief  Default destructor */
    virtual ~CCardSupport() = default;

    /** \brief  Standard constructor */
    CCardSupport(int id, std::string name, int health, int armor, int strength);

    /** \brief Use a support special ability on a specified card */
    void Special(CCard *card) override;

    /** \brief Use a support special ability on a specified player  */
    void Special(CPlayer *player) override;

    /** \brief  Attack a specified player */
    bool Attack(CPlayer *player) override;

    /** \brief Prints itself to a specified window */
    void Print(WINDOW *window, int index) const override;
};


#endif

