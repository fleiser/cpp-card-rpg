#ifndef CCARDDEFENSIVE_H
#define CCARDDEFENSIVE_H

#include "CCard.h"
#include <string>
#include <ncurses.h>
#include "CPlayer.h"

/** \brief Class represents cards with defensive qualities*/
class CCardDefensive : public CCard {
public:
    /** \brief  Default destructor */
    virtual ~CCardDefensive() = default;

    /** \brief  Standard constructor */
    CCardDefensive(int id, std::string name, int health, int armor, int strength);

    /** \brief  Constructor for a defensive card */
    void Special(CCard *card) override;

    /** \brief Use a defensive special ability on a specified player  */
    void Special(CPlayer *player) override;

    /** \brief  Attack a specified player */
    bool Attack(CPlayer *player) override;

    /** \brief Prints itself to a specified window */
    void Print(WINDOW *window, int index) const override;
};


#endif

