#ifndef CCARDOFFENSIVE_H
#define CCARDOFFENSIVE_H

#include "CCard.h"
#include <string>
#include "CPlayer.h"
#include <ncurses.h>

/** \brief Class represents cards with offensive qualities*/
class CCardOffensive : public CCard {
public:
    /** \brief  Default destructor */
    virtual ~CCardOffensive() = default;

    /** \brief  Standard constructor */
    CCardOffensive(int id, std::string name, int health, int armor, int strength);

    /** \brief  Constructor for a offensive card */
    void Special(CCard *card) override;

    /** \brief Use a offensive special ability on a specified player  */
    void Special(CPlayer *player) override;

    /** \brief  Attack a specified player */
    bool Attack(CPlayer *player) override;

    /** \brief Prints itself to a specified window */
    void Print(WINDOW *window, int index) const override;
};

#endif

