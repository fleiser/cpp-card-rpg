#include "CCard.h"
#include "CCardOffensive.h"

CCardOffensive::CCardOffensive(int id, std::string name, int health, int armor, int strength) {
    m_id = id;
    m_name = name;
    m_health = health;
    m_armor = armor;
    m_strength = strength;
}

void CCardOffensive::Special(CCard *card) {
    card->SetStrength(card->GetStrength() + 5);
    SetHealth(GetHealth() - 2);
    if (m_owner != nullptr) {
        m_owner->GetGame()->InsertToLog(m_name + " increased " + card->GetName() + " damage by 5, suffering 2 damage");
    }
}

void CCardOffensive::Special(CPlayer *player) {
    player->SetArmor(player->GetArmor() + 10);
    SetHealth(GetHealth() - 2);
    if (m_owner != nullptr) {
        m_owner->GetGame()->InsertToLog(
                m_name + " increased " + player->GetName() + " armor by 10, suffering 2 damage");
    }
}

bool CCardOffensive::Attack(CPlayer *player) {
    int afterArmor = (m_strength) * (m_health / 4) - player->GetArmor();
    if (afterArmor < 0) {
        afterArmor = 0;
    }
    player->SetArmor(player->GetArmor() - (m_strength) * (m_health / 4));
    if (m_owner != nullptr) {
        m_owner->GetGame()->InsertToLog(
                m_name + " attacks " + player->GetName() + " dealing " + std::to_string(afterArmor) + " damage");
    }
    return player->SetHealth(player->GetHealth() - afterArmor);
}

void CCardOffensive::Print(WINDOW *window, int index) const {
    mvwprintw(window, index, 3, "-A %s h: %i | a: %i | s: %i ", m_name.c_str(), m_health, m_armor, m_strength);
}

