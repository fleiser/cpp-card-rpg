#ifndef CAI_H
#define CAI_H

#include "CPlayer.h"
#include <string>

/** \brief Class representing an "AI" */
class CAI : public CPlayer {
public:
    /** \brief Standard constructor */
    CAI(std::string name, std::vector<CCard *> cards, int health, int armor, int energy, int usedEnergy, CGame *game);

    /** \brief Loading constructor */
    CAI(std::string name, int health, int armor, int energy, int usedEnergy, CGame *game);

    /** \brief Destructor */
    virtual  ~CAI();

    /** \brief AI plays */
    void Play() override;

private:
    int m_fullHealth = 300;

    /** \brief  Aggressive behaviour*/
    void Attack(bool player);

    /** \brief  Defensive behaviour */
    void Defend();

};

#endif

