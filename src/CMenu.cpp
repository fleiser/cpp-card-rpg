#include "CMenu.h"
#include "CCardOffensive.h"
#include "CCardDefensive.h"
#include "CCardSupport.h"
#include <ncurses.h>
#include <fstream>
#include <sstream>
#include <algorithm>

CMenu::CMenu() {
    bool cards = LoadCards();
    bool decks = LoadDecks();
    if (!cards) {
        std::cout << "FAILED TO LOAD CARDS" << std::endl;
    }
    if (!decks) {
        std::cout << "FAILED TO LOAD DECKS" << std::endl;
    }
}

CMenu::~CMenu() {
    for (auto &it:m_cards) {
        delete it.second;
    }
    m_cards.clear();
}

void CMenu::Play() {
    int height = 9;
    int width = 20;
    int x = 1;
    int y = 1;
    //Window
    echo();
    WINDOW *gameWin = newwin(height, width, y, x);
    box(gameWin, 0, 0);
    refresh();
    mvwprintw(gameWin, 1, 2, "Player 1 name:");
    char playerName[25];
    mvwgetnstr(gameWin, 2, 2, playerName, 24);
    wclear(gameWin);
    wrefresh(gameWin);
    delwin(gameWin);
    noecho();
    std::vector<CCard *> deckOne;
    SelectDeck(&deckOne);
    if (deckOne.empty()) {
        std::cout << "FAILED EMPTY!" << std::endl;
        return;
    }
    echo();
    gameWin = newwin(height, width, y, x);
    box(gameWin, 0, 0);
    refresh();
    mvwprintw(gameWin, 1, 2, "Player 2 name:");
    char computerName[25];
    mvwgetnstr(gameWin, 2, 2, computerName, 24);
    wclear(gameWin);
    wrefresh(gameWin);
    delwin(gameWin);
    noecho();
    std::vector<CCard *> deckTwo;
    SelectDeck(&deckTwo);
    if (deckTwo.empty()) {
        std::cout << "FAILED EMPTY!" << std::endl;
        return;
    }
    while (!ScreenOk()) {}
    CGame game(playerName, deckOne, computerName, deckTwo, true, false);
    game.Play();
}

void CMenu::PlayVsPc() {
    int height = 9;
    int width = 20;
    int x = 1;
    int y = 1;
    //Window
    echo();
    WINDOW *gameWin = newwin(height, width, y, x);
    box(gameWin, 0, 0);
    refresh();
    mvwprintw(gameWin, 1, 2, "Player name:");
    char playerName[25];
    mvwgetnstr(gameWin, 2, 2, playerName, 24);
    wclear(gameWin);
    wrefresh(gameWin);
    delwin(gameWin);
    noecho();
    std::vector<CCard *> deckOne;
    SelectDeck(&deckOne);
    if (deckOne.empty()) {
        std::cout << "FAILED EMPTY!" << std::endl;
        return;
    }
    echo();
    gameWin = newwin(height, width, y, x);
    box(gameWin, 0, 0);
    refresh();
    mvwprintw(gameWin, 1, 2, "Computer name:");
    char computerName[25];
    mvwgetnstr(gameWin, 2, 2, computerName, 24);
    wclear(gameWin);
    wrefresh(gameWin);
    delwin(gameWin);
    noecho();
    std::vector<CCard *> deckTwo;
    SelectDeck(&deckTwo);
    if (deckTwo.empty()) {
        std::cout << "FAILED EMPTY!" << std::endl;
        return;
    }
    while (!ScreenOk()) {}
    CGame game(playerName, deckOne, computerName, deckTwo, false, false);
    game.Play();
}

void CMenu::ResumeGame(std::string game) {
    while (!ScreenOk()) {}
    CGame savedGame(game);
    savedGame.Play();
}

void CMenu::LoadGames() {
    std::vector<std::string> gameNames;
    std::ifstream stream("examples/saves.csv");
    if (stream.is_open()) {
        std::string line;
        while (std::getline(stream, line)) {
            if (std::find(gameNames.begin(), gameNames.end(), line) == gameNames.end())
                gameNames.push_back(line);
        }
    }
    if (gameNames.size() <= 0) {
        return;
    }
    auto height = static_cast<int>(2 + gameNames.size());
    int width = 20;
    int xMenu = 2;
    int yMenu = 1;
    WINDOW *loadMenu = newwin(height, width, xMenu, yMenu);
    box(loadMenu, 0, 0);
    refresh();
    keypad(loadMenu, true);
    int selected = 0;
    int keyValue;
    while (true) {
        for (int i = 0; i < static_cast<int>(gameNames.size()); ++i) {
            if (i == selected)
                wattron(loadMenu, A_REVERSE);
            mvwprintw(loadMenu, i + 1, 2, gameNames[i].c_str());
            wattroff(loadMenu, A_REVERSE);
        }
        keyValue = wgetch(loadMenu);
        switch (keyValue) {
            case KEY_UP:
                selected--;
                if (selected == -1)
                    selected = 0;
                break;
            case KEY_DOWN:
                selected++;
                if (selected == static_cast<int>(gameNames.size()))
                    selected = static_cast<int>(gameNames.size() - 1);
                break;
            default:
                break;
        }
        if (keyValue == 10) {
            break;
        }
        refresh();
    }
    wclear(loadMenu);
    wrefresh(loadMenu);
    delwin(loadMenu);
    ResumeGame(gameNames[selected]);
}

bool CMenu::LoadDecks() {
    m_decks.clear();
    std::string line;
    std::ifstream stream;
    stream.open("examples/decks.csv");
    if (!stream.is_open()) {
        return false;
    }
    while (std::getline(stream, line)) {
        std::vector<std::string> values;
        std::stringstream ss(line);
        while (ss.good()) {
            std::string sub;
            getline(ss, sub, ',');
            values.push_back(sub);
        }
        if (values.size() != 6) {
            return false;
        }
        std::vector<CCard *> deck;
        std::string name = values[0];
        for (int i = 1; i < 6; ++i) {
            int id = std::stoi(values[i]);
            auto it = m_cards.find(id);
            if (it != m_cards.end()) {
                CCard *card = (*it).second;
                deck.push_back(card);
            } else {
                return false;
            }
        }
        m_decks.insert({name, deck});
    }
    return true;
}

bool CMenu::LoadCards() {
    for (auto &it:m_cards) {
        delete it.second;
    }
    m_cards.clear();
    std::string line;
    std::ifstream stream;
    stream.open("examples/cards.csv");
    if (!stream.is_open()) {
        return false;
    }
    while (std::getline(stream, line)) {
        std::vector<std::string> values;
        std::stringstream ss(line);
        while (ss.good()) {
            std::string sub;
            getline(ss, sub, ',');
            values.push_back(sub);
        }
        if (values.size() != 6) {
            return false;
        }
        CCard *card = nullptr;
        int id = std::stoi(values[0]);
        int cardType = std::stoi(values[1]);
        std::string name = values[2];
        int health = std::stoi(values[3]);
        int armor = std::stoi(values[4]);
        int strength = std::stoi(values[5]);
        switch (cardType) {
            case 0:
                card = new CCardOffensive(id, name, health, armor, strength);
                break;
            case 1:
                card = new CCardDefensive(id, name, health, armor, strength);
                break;
            case 2:
                card = new CCardSupport(id, name, health, armor, strength);
                break;
            default:
                break;
        }
        if (card == nullptr) {
            return false;
        }
        m_cards.insert({card->GetId(), card});
    }
    return true;

}

void CMenu::ShowMenu() {
    while (!ScreenOk()) {}
    int height = 9;
    int width = 20;
    int xMenu = 1;
    int yMenu = 1;
    //Window
    WINDOW *menuWin = newwin(height, width, yMenu, xMenu);
    box(menuWin, 0, 0);
    refresh();
    wrefresh(menuWin);
    keypad(menuWin, true);
    //Main menu
    int numberOfOptions = 4;
    std::string options[4] = {"Play vs PC", "Multiplayer", "Load game", "Exit"};
    int selected = 0;
    int keyValue;
    while (true) {
        for (int i = 0; i < numberOfOptions; ++i) {
            if (i == selected)
                wattron(menuWin, A_REVERSE);
            mvwprintw(menuWin, i + 1, 2, options[i].c_str());
            wattroff(menuWin, A_REVERSE);
        }
        keyValue = wgetch(menuWin);
        switch (keyValue) {
            case KEY_UP:
                selected--;
                if (selected == -1)
                    selected = 0;
                break;
            case KEY_DOWN:
                selected++;
                if (selected == numberOfOptions)
                    selected = numberOfOptions - 1;
                break;
            default:
                break;
        }
        if (keyValue == 10) {
            break;
        }
        refresh();
    }
    wclear(menuWin);
    wrefresh(menuWin);
    delwin(menuWin);
    switch (selected) {
        case 0: {
            PlayVsPc();
            break;
        }
        case 1: {
            Play();
            break;
        }
        case 2: {
            LoadGames();
            break;
        }
        case 3: {
            return;
        }
        default:
            return;
    }
    ShowMenu();
}

void CMenu::SelectDeck(std::vector<CCard *> *rDeck) {
    while (!ScreenOk()) {}
    auto height = static_cast<int>(2 + m_decks.size());
    int width = 20;
    int xMenu = 1;
    int yMenu = 1;
    //Window
    WINDOW *deckWin = newwin(height, width, yMenu, xMenu);
    box(deckWin, 0, 0);
    refresh();
    wrefresh(deckWin);
    keypad(deckWin, true);
    auto numberOfOptions = static_cast<int>(m_decks.size());
    std::vector<std::string> localDecks;
    int selected = 0;
    int keyValue;

    while (true) {
        int i = 0;
        for (const auto &deck:m_decks) {
            if (i == selected)
                wattron(deckWin, A_REVERSE);
            mvwprintw(deckWin, i + 1, 2, deck.first.c_str());
            localDecks.push_back(deck.first);
            wattroff(deckWin, A_REVERSE);
            i++;
        }
        keyValue = wgetch(deckWin);
        switch (keyValue) {
            case KEY_UP:
                selected--;
                if (selected == -1)
                    selected = 0;
                break;
            case KEY_DOWN:
                selected++;
                if (selected == numberOfOptions)
                    selected = numberOfOptions - 1;
                break;
            default:
                break;
        }
        if (keyValue == 10) {
            break;
        }
        refresh();
    }
    wclear(deckWin);
    wrefresh(deckWin);
    delwin(deckWin);
    auto selectedDeck = m_decks.find(localDecks[selected]);
    for (const auto &it:(*selectedDeck).second) {
        auto iterator = m_cards.find(it->GetId());
        if (iterator != m_cards.end()) {
            auto card = (*iterator).second;
            if (dynamic_cast<CCardOffensive *>(card)) {
                (*rDeck).push_back(
                        new CCardOffensive(card->GetId(), card->GetName(), card->GetHealth(), card->GetArmor(),
                                           card->GetStrength()));
                continue;
            }
            if (dynamic_cast<CCardDefensive *>(card)) {
                (*rDeck).push_back(
                        new CCardDefensive(card->GetId(), card->GetName(), card->GetHealth(), card->GetArmor(),
                                           card->GetStrength()));
                continue;
            }
            if (dynamic_cast<CCardSupport *>(card)) {
                (*rDeck).push_back(new CCardSupport(card->GetId(), card->GetName(), card->GetHealth(), card->GetArmor(),
                                                    card->GetStrength()));
                continue;
            }
        } else {
            return;
        }
    }

}

bool CMenu::ScreenOk() {
    wclear(stdscr);
    refresh();
    int w = 0;
    int h = 0;
    getmaxyx(stdscr, h, w);
    if (h < 23 || w < 108) {
        if (w > 108) { w = 108; }
        if (h > 24) { h = 24; }
        mvwprintw(stdscr, 0, 0, "Please incrase the size of your terminal by %i rows and %i columns and press any key",
                  24 - h, 108 - w);
        wgetch(stdscr);
        return false;
    }
    return true;
}
