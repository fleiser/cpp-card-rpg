#ifndef CPLAYER_H
#define CPLAYER_H

#include "CCard.h"
#include "CGame.h"
#include <vector>
#include <string>

class CGame;

class CCard;

/** \brief Class serves as a template for a player which can be either human or an "AI" */
class CPlayer {
public:
    virtual void Play()=0;

    virtual ~CPlayer() = 0;

    /** \brief  Returns the players health */
    int GetHealth();

    /** \brief  Returns the players armor */
    int GetArmor();

    /** \brief  Returns players used energy */
    int GetUsedEnergy();

    /** \brief  Returns players total energy*/
    int GetEnergy();

    /** \brief  Returns the players deck */
    std::vector<CCard *> GetDeck();

    /** \brief  Sets the players deck*/
    void SetDeck(std::vector<CCard *> cards);

    /** \brief  Sets the players health */
    bool SetHealth(int health);

    /** \brief  Sets the players used energy */
    void SetUsedEnergy(int used);

    /** \brief  Sets the players armor  */
    void SetArmor(int armor);

    /** \brief  Sets the players energy */
    void SetEnergy(int energy);

    /** \brief  Removes the specified card from players deck */
    void RemoveFromDeck(CCard *card);

    /** \brief  Get players name */
    std::string GetName();

    /** \brief  Create a ncurses window displaying the player */
    WINDOW *ShowPlayer(CPlayer *player, int w, int h, int x, int y);

    /** \brief  Returns the game played by this player */
    CGame *GetGame();

protected:
    int m_health = 300;
    int m_energy = 1;
    int m_usedEnergy = 0;
    int m_armor = 100;
    std::vector<CCard *> m_deck;
    std::string m_name;
    CGame *m_game = nullptr;
};


#endif

