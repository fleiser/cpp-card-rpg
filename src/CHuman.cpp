#include "CHuman.h"
#include <ncurses.h>

CHuman::CHuman(std::string name, std::vector<CCard *> cards, int health, int armor, int energy, int usedEnergy,
               CGame *game) {
    m_name = name;
    m_deck = cards;
    m_health = health;
    m_armor = armor;
    m_game = game;
    m_usedEnergy = usedEnergy;
    m_energy = energy;
}

CHuman::CHuman(std::string name, int health, int armor, int energy, int usedEnergy, CGame *game) {
    m_name = name;
    m_health = health;
    m_armor = armor;
    m_game = game;
    m_usedEnergy = usedEnergy;
    m_energy = energy;
}


void CHuman::Play() {
    m_game->ClearCards();
    if (m_game->GameOver()) {
        return;
    }
    while (m_usedEnergy < m_energy) {
        WINDOW *player = ShowPlayer(this, 54, 15, 1, 1);
        WINDOW *enemy = ShowPlayer(m_game->GetEnemyPlayer(this), 54, 15, 55, 1);
        ActionMenu();
        m_game->ClearCards();
        if (m_game->GameOver()) {
            return;
        }
        delwin(player);
        delwin(enemy);
        m_usedEnergy++;
    }
    m_usedEnergy = 0;
    /*for (int i = m_energy; i > 0; --i) {
        WINDOW *player = ShowPlayer(this, 54, 15, 1, 1);
        WINDOW *enemy = ShowPlayer(m_game->GetEnemyPlayer(this), 54, 15, 55, 1);
        ActionMenu();
        m_game->ClearCards();
        if (m_game->GameOver()) {
            return;
        }
        delwin(player);
        delwin(enemy);
    }*/
}

void CHuman::ActionMenu() {
    int aX = 1;
    int aY = 16;
    WINDOW *actionWindow = newwin(8, 25, aY, aX);
    box(actionWindow, 0, 0);
    refresh();
    wrefresh(actionWindow);
    keypad(actionWindow, true);
    int numberOfOptions = 6;
    std::string options[6] = {"Attack enemy player", "Attack card", "Special", "Special on Player", "Save Game",
                              "Exit"};
    int selected = 0;
    int keyValue;
    while (true) {
        for (int i = 0; i < numberOfOptions; ++i) {
            if (i == selected)
                wattron(actionWindow, A_REVERSE);
            mvwprintw(actionWindow, i + 1, 2, options[i].c_str());
            wattroff(actionWindow, A_REVERSE);
        }
        keyValue = wgetch(actionWindow);
        switch (keyValue) {
            case KEY_UP:
                selected--;
                if (selected == -1)
                    selected = 0;
                break;
            case KEY_DOWN:
                selected++;
                if (selected == numberOfOptions)
                    selected = numberOfOptions - 1;
                break;
            default:
                break;
        }
        if (keyValue == 10) {
            break;
        }
        refresh();
    }
    wclear(actionWindow);
    wrefresh(actionWindow);
    delwin(actionWindow);
    //refresh();
    switch (selected) {
        case 0: {
            //Attack Menu
            CCard *card = SelectCardMenu(this);
            card->Attack(m_game->GetEnemyPlayer(this));
            break;
        }
        case 1: {
            //Attack Menu
            CCard *card = SelectCardMenu(this);
            CCard *enemyCard = SelectCardMenu(m_game->GetEnemyPlayer(this));
            card->Attack(enemyCard);
            break;
        }
        case 2: {
            //Special Menu
            CCard *card = SelectCardMenu(this);
            CCard *enemyCard = SelectCardMenu(this);
            card->Special(enemyCard);
            break;
        }
        case 3: {
            //Special Menu
            CCard *card = SelectCardMenu(this);
            card->Special(this);
            break;
        }
        case 4: {
            //Save
            //m_energy++;
            m_game->SaveGame();
            ActionMenu();
            break;
        }
        case 5: {
            //Surrender
            m_health = 0;
            break;
        }
        default:
            break;
    }
}

CCard *CHuman::SelectCardMenu(CPlayer *player) {
    int aX = 1;
    int aY = 16;
    WINDOW *selectWindow = newwin(static_cast<int>(2 + player->GetDeck().size()), 25, aY, aX);
    box(selectWindow, 0, 0);
    refresh();
    wrefresh(selectWindow);
    keypad(selectWindow, true);
    int selected = 0;
    int keyValue;
    while (true) {
        for (int i = 0; i < static_cast<int>(player->GetDeck().size()); ++i) {
            if (i == selected)
                wattron(selectWindow, A_REVERSE);
            mvwprintw(selectWindow, i + 1, 2, player->GetDeck()[i]->GetName().c_str());
            wattroff(selectWindow, A_REVERSE);
        }
        keyValue = wgetch(selectWindow);
        switch (keyValue) {
            case KEY_UP:
                if (selected != 0)
                    selected--;
                break;
            case KEY_DOWN:
                selected++;
                if (selected == static_cast<int>(player->GetDeck().size()))
                    selected = static_cast<int>(player->GetDeck().size() - 1);
                break;
            default:
                break;
        }
        if (keyValue == 10) {
            break;
        }
        refresh();
    }
    wclear(selectWindow);
    wrefresh(selectWindow);
    delwin(selectWindow);
    return player->GetDeck()[selected];
}

CHuman::~CHuman() {
    for (auto &item:m_deck) {
        delete item;
    }
    m_deck.clear();
}

