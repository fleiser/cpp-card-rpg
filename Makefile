CXX=	g++
OCPPFLAGS= -lncurses
CPPFLAGS=  -std=c++14 -Wall -pedantic -Wno-long-long -O0 -ggdb
SRC=src/

compile:	$(SRC)main.o	$(SRC)CMenu.o $(SRC)CCard.o	$(SRC)CCardOffensive.o	$(SRC)CCardDefensive.o	$(SRC)CCardSupport.o	$(SRC)CPlayer.o	$(SRC)CHuman.o	$(SRC)CGame.o	$(SRC)CAI.o
	$(CXX)	$(CPPFLAGS)    $(OCPPFLAGS)	$(SRC)main.o	$(SRC)CMenu.o $(SRC)CCard.o	$(SRC)CCardOffensive.o	$(SRC)CCardDefensive.o	$(SRC)CCardSupport.o	$(SRC)CPlayer.o	$(SRC)CHuman.o	$(SRC)CGame.o	$(SRC)CAI.o	-o	fleisja6

main.o:	$(SRC)main.cpp
	$(CXX)   $(CPPFLAGS)	-c	$(SRC)main.cpp

CHuman.o:	$(SRC)CHuman.cpp  $(SRC)CHuman.h
	$(CXX)	$(CPPFLAGS)    -c	$(SRC)CHuman.cpp	$(SRC)CHuman.h

CPlayer.o:	$(SRC)CPlayer.cpp $(SRC)CPlayer.h
	$(CXX)	$(CPPFLAGS)    -c	$(SRC)CPlayer.cpp	$(SRC)CPlayer.h

CCard.o:	$(SRC)CXXard.cpp   $(SRC)CXXard.h
	$(CXX)	$(CPPFLAGS)    -c	$(SRC)CXXard.cpp	$(SRC)CXXard.h

CXXardOffensive.o:	$(SRC)CXXardOffensive.cpp  $(SRC)CXXardOffensive.h
	$(CXX)	$(CPPFLAGS)    -c	$(SRC)CXXardOffensive.cpp	$(SRC)CXXardOffensive.h

CXXardDefensive.o:	$(SRC)CXXardDefensive.cpp  $(SRC)CXXardDefensive.h
	$(CXX)	$(CPPFLAGS)    -c	$(SRC)CXXardDefensive.cpp	$(SRC)CXXardDefensive.h

CXXardSupport.o:	$(SRC)CXXardSupport.cpp    $(SRC)CXXardSupport.h
	$(CXX)	$(CPPFLAGS)    -c	$(SRC)CXXardSupport.cpp	$(SRC)CXXardSupport.h

CAI.o:	$(SRC)CAI.cpp $(SRC)CAI.h
	$(CXX)	$(CPPFLAGS)    -c	$(SRC)CAI.cpp	$(SRC)CAI.h

CGame.o:	$(SRC)CGame.cpp   $(SRC)CGame.h
	$(CXX)	$(CPPFLAGS)    -c	$(SRC)CGame.cpp	$(SRC)CGame.h

CMenu.o:	$(SRC)CMenu.cpp   $(SRC)CMenu.h
	$(CXX)	$(CPPFLAGS)    -c	$(SRC)CMenu.cpp	$(SRC)CMenu.h
doc:
	doxygen	doxygen.config
run:
	./fleisja6
clean:
	rm	-f	$(SRC)*.o
	rm	-f	fleisja6
	rm	-rf	doc
